k8sapp-audit
===================

This project contains StarlingX Kubernetes application specific python plugins
for auditd. These plugins are required to integrate the auditd application into
the StarlingX application framework and to support the various StarlingX
deployments.
