#
# Copyright (c) 2021 Wind River Systems, Inc.
#
# SPDX-License-Identifier: Apache-2.0
#

# Application Name
HELM_APP_AUDITD = 'auditd'

# Namespace to deploy the application
HELM_NS_AUDITD = 'kube-system'

# Helm: Supported charts:
# These values match the names in the chart package's Chart.yaml
HELM_CHART_AUDITD = 'auditd'
